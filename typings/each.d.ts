declare type callBack = (args: Promise<any> | any) => Promise<any> | any;
declare type myIterable = Promise<any>[] | any[] | any;
export declare function each(iterable: myIterable, cb: callBack): Promise<any>;
export {};
