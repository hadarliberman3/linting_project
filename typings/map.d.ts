declare type callBack = (...args: Promise<any> | any) => Promise<any> | any;
declare type myIterable = Promise<any>[] | any;
export declare function mapParallel(promises: myIterable, cb: callBack): Promise<any[]>;
export declare function mapSeries(promises: myIterable, cb: callBack): Promise<any[]>;
export {};
