declare type myIterable = Promise<any>[] | any[];
export declare function some(promises: myIterable, num: number): Promise<any>;
export {};
