declare type myIterable = Promise<any>[] | any[];
export declare function race(promises: myIterable): Promise<any> | any;
export {};
