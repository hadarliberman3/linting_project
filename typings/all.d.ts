declare type myIterable = Promise<any> | any;
export declare function all(iterable: myIterable[]): Promise<any[]>;
export {};
