declare type callBack = (...args: Promise<any> | any) => Promise<any> | any;
declare type myIterable = Promise<any>[] | any[];
export declare function reduce(iterable: myIterable, callBack: callBack, initialise: any): Promise<any>;
export {};
