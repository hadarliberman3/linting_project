interface IPromiseObj {
    [key: string]: Promise<any> | any;
}
export declare function props(promisesObj: IPromiseObj): Promise<IPromiseObj>;
export {};
