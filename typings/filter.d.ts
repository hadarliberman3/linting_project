declare type callBack = (args: Promise<any> | any) => Promise<any> | any;
declare type myIterable = Promise<any>[] | any;
export declare function filterSeries(iterable: myIterable, cb: callBack): Promise<string | any[]>;
export declare function filterParallel(iterable: myIterable, cb: callBack): Promise<string | any[]>;
export {};
