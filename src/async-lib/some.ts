
type myIterable=Promise<any>[]|any[];
export async function some(promises:myIterable,num:number):Promise<any>{
    const results = [] as Promise<any>[];
    return await new Promise((resolve)=>{
        promises.forEach(async p => {
            results.push(await p);
            if(results.length === num) resolve(results); 
        });
    });
}