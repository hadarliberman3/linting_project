type myIterable=Promise<any>[]|any[];

export function race(promises:myIterable):Promise<any>|any{
    return new Promise((resolve, reject) => {
         promises.forEach(p => p.then(resolve).catch(reject));
 });
     
 }