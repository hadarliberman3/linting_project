type callBack = (...args: Promise<any> | any) => Promise<any> | any;
type myIterable=Promise<any>[]|any;

export async function mapParallel(
    promises: myIterable,
    cb: callBack
): Promise<any[]> {
    const pendingAfterCb = [];
    for (const promise of promises) {
        pendingAfterCb.push(cb(promise));
    }
    const results = [];
    for (const promise of pendingAfterCb) {
        results.push(await promise);
    }
    return results;
}

export async function mapSeries(
    promises: myIterable,
    cb: callBack
): Promise<any[]> {
    const results = [];
    for (const promise of promises) {
        results.push(await cb(await promise));
    }

    return results;
}