interface IPromiseObj{
    [key:string]:Promise<any>|any
}

export async function props(promisesObj:IPromiseObj):Promise<IPromiseObj> {
    const results:IPromiseObj = {};
    for (const key in promisesObj) {
      results[key] = await promisesObj[key];
    }  
    return results;
  }