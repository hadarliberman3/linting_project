export function mydelay(ms: number): Promise<any> {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
