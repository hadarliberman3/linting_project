type callBack = (...args: Promise<any> | any) => Promise<any> | any;
type myIterable=Promise<any>[]|any[];



export async function reduce(iterable:myIterable,callBack:callBack,initialise:any){
    let val=await initialise || await iterable.shift();
    for(let cur of iterable){
            cur=await cur;
            val=await callBack(val,cur);       
        }
        return val;      
}