type callBack = (args: Promise<any>|any) => Promise<any> | any;
type myIterable=Promise<any>[]|any[]|any;
export async function each(iterable:myIterable, cb:callBack):Promise<any>{
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}
