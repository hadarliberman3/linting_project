
type myIterable = Promise<any> | any;  


export async function all(iterable: myIterable[]): Promise<any[]> {
    // take an iterable
    // `all` returns a promise.
    const results = [];
    for (const promise of iterable) {
        results.push(await promise);
    }
    return results;
}

// all([promise2,promise3,promise1]).then((resolve)=>{
//     console.log(resolve);
// });
